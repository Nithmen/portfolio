import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery/dist/jquery.min.js'

import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  data: {
    a : 1
  }
}).$mount('#app')
